#include <LPC21xx.H>
#define NULL 0

void UIntToHexStr(unsigned int uiValue, char pcStr[]){

	unsigned char ucNibbleCounter;
	unsigned char ucCurrentNibble;

	pcStr[0]='0';
	pcStr[1]='x';
	pcStr[6]=NULL;

	for(ucNibbleCounter=0; ucNibbleCounter<4; ucNibbleCounter++){
		ucCurrentNibble=(uiValue >> (ucNibbleCounter*4))&0x0F;
		if(ucCurrentNibble<10){
			pcStr[5-ucNibbleCounter] = '0'+ucCurrentNibble;
		}
		else{
			pcStr[5-ucNibbleCounter] = 'A'+(ucCurrentNibble-10);
		}
	}
}

enum Result {OK, ERROR};

enum Result eHexStringToUInt(char pcStr[],unsigned int *puiValue){

	unsigned char ucCharacterCounter;
	unsigned char ucCurrentCharacter;
	*puiValue=0;

	if((pcStr[0]!='0') || (pcStr[1]!='x') || (pcStr[2]==NULL)){
	return ERROR;
	}
	for(ucCharacterCounter=2; ucCharacterCounter<7; ucCharacterCounter++){
		ucCurrentCharacter = pcStr[ucCharacterCounter];
		if(ucCurrentCharacter==NULL){
		return OK;
		}
		else if(ucCharacterCounter==6){
			return ERROR;
		}
		*puiValue = *puiValue << 4;
		if(ucCurrentCharacter <= '9' && ucCurrentCharacter >= '0'){
			*puiValue = *puiValue | (ucCurrentCharacter-'0');
		}
		else if(ucCurrentCharacter <= 'F' && ucCurrentCharacter >= 'A'){
			*puiValue = *puiValue | (ucCurrentCharacter-'A'+10);
		}
		else{
			return ERROR;
		}
	}
	return ERROR;
}

void AppendUIntToString(unsigned int uiValue, char pcDestinationStr[]){

	unsigned char ucIndeksKonca;

	for(ucIndeksKonca=0; pcDestinationStr[ucIndeksKonca]!=NULL; ucIndeksKonca++){}
	UIntToHexStr(uiValue, &pcDestinationStr[ucIndeksKonca]);
}

/*int main(){
	 //2 unsigned int  uiValue;
	 //2 enum Result eResult = eHexStringToUInt("0xC",&uiValue);
	 //1 char pcStr[7];
	 //1  UIntToHexStr(12, pcStr);
	 //3  char pcStr[10] = "abcd";
	 //3  AppendUIntToString(12, pcStr);
	
	unsigned int uiValue;
	char pcRightFormat[] = "0xABCD";
	char pcTooLong[] = "0x000CD";
	char pcTooShort[] = "0x";
	char pcWrongPrefix[] = "0bFFFF";
	enum Result eResultRightFormat = eHexStringToUInt(pcRightFormat,&uiValue);
	enum Result eResultTooLong = eHexStringToUInt(pcTooLong,&uiValue);
	enum Result eResultTooShort = eHexStringToUInt(pcTooShort,&uiValue);
	enum Result eResultWrongPrefix = eHexStringToUInt(pcWrongPrefix,&uiValue);

	
}
*/
